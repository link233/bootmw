# Bootstrap Mediawiki Bundle
A MediaWiki bundle with Bootstrap, FontAwesome and some useful extensions.  
<br/>
·[Download](#)·[Install](#)·[Extensions](#)·[Skins](http://bootswatch.com/)·[Help](#)·[Wikis](#)·  
## Contributors of this project
<li> [BeckNo9](http://www.bn9.in)</li><br/>
<hr/><p><big>Join this project?</big></p>
<p> First send a mail to me: <a href="mail-to:me@bn9.in">Send a mail</a></p>
<p> Tell me why you want to join the develop group in your email.</p>
<p> Wait for my reply.</p>